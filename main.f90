program main

use nrtype
use Int_monteCarlo
!use Funciones

implicit none

integer(I4B)    :: n,i
real            :: a, b, r

intrinsic       ::sin

call iniciar_semilla()


r=Int1DMonteCarlo(0.0,1.0,sin,10000)

print *, 'resultado', r


contains

function sen(x)
    real             ::sen
    real, intent(in) ::x

sen=sin(x)
endfunction
endprogram main
