!module Funciones

!interface
!    function sen(x)
!        real                ::sen
!        real, intent(in)    :: x
!    endfunction
!endinterface
!
!contains
!
!function sen(x)
!    real             ::sen
!    real, intent(in) ::x
!
!sen=sin(x)
!endfunction
!endmodule Funciones



module Int_MonteCarlo

    implicit none

!
!Subrutinas
!
contains
!Subrutina de iniciacion de semilla
subroutine iniciar_semilla()
    use nrtype
    implicit none

    integer(I4B)                          :: n, clock
    integer(I1B)                          :: i 
    integer, dimension(:), allocatable    :: seed

    call random_seed(size = n)
    allocate(seed(n))

    call system_clock(count=clock)
    
    seed = clock + 37 * (/ (i - 1, i = 1, n) /)
    call random_seed(put = seed)

    deallocate(seed)
endsubroutine iniciar_semilla
!
!funcion de integracioun unidimensional por MonteCarlo
function Int1DMonteCarlo(aF,bF,f,nF) result (res)

    implicit none

    real, intent(in)                :: aF, bF ! Extremo inferior y superior de integracion
    integer, optional, intent(in)   :: nF    ! Numero de muestras
    real                            :: a, b
    integer                         :: n
    real                            :: res, aux, Xaux, Yaux, Ymin, Ymax, area
    integer                         :: i, contI

    interface
         function f(x)
            implicit none
            real, intent(in)    ::x
            real                ::f
         endfunction
    endinterface

!Algoritmo

!Asignaciones que permiten conservar intent(in)
a=aF; b=bF; n=nF

!Doy valor a n si no esta como parametro
    if(.not.present(nF)) n=100*(1+(b-a))

!Inicio otros valores
Ymin=0; Ymax=0; contI=0;

!Intercambio a y b si estan invertidas
    if(a>b) then
        aux=b; b=a; a=aux
    endif

!Doy valores a Ymin y Ymax
    !TODO: Funcion que me devuelva el maximo y el minimo de una funcion
    Ymin=-2; Ymax=2
!Calculo de la integral
    do, i=1,n
        call random_number(Xaux)
        call random_number(Yaux)
        Xaux=a+(b-a)*Xaux
        Yaux=Ymin+(Ymax-Ymin)*Yaux
        aux=f(Xaux)

        if ((Yaux<aux).and.(Yaux>0.0)) then
            contI=contI+1.0
        elseif((Yaux<0.0).and.(Yaux>aux)) then
            contI=contI-1.0
        endif

    enddo

area=(b-a)*(Ymax-Ymin)

res=area*contI/n

endfunction Int1DMonteCarlo
!--Fin--



endmodule Int_MonteCarlo
